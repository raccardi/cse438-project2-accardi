package com.example.playlist

import android.annotation.SuppressLint
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
//not sure what this does, credit to
//https://stackoverflow.com/questions/46419171/how-to-fetch-resource-id-in-fragment-using-kotlin-in-android
import kotlinx.android.synthetic.main.fragment_main.view.*
import kotlinx.android.synthetic.main.fragment_main.*
import kotlinx.android.synthetic.main.song_list_item.*
import android.R
import android.content.Intent
import android.os.AsyncTask
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.widget.*
import com.example.playlist.PlaylistSupport.Song
import com.example.playlist.PlaylistSupport.*
import com.squareup.picasso.Picasso
import java.text.NumberFormat


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [AddJokeForm.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [AddJokeForm.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class TopTracks() : Fragment() {
    private var listener: OnFragmentInteractionListener? = null
    private val LogTag = this::class.java.simpleName

    private lateinit var songGrid: GridView;
    private var songList: ArrayList<PlaylistSupport.Song>? = arrayListOf<PlaylistSupport.Song>();
    private val numCols = 2;

    /*
    //shoutout to Studio 3
    inner class SongAdapter: BaseAdapter() {

        override fun getView(p0: ViewGroup, p1: Int): SongViewHolder {
            val itemView = LayoutInflater.from(p0.context).inflate(R.layout.song_list_item, p0, false) //TODO
            return SongViewHolder(itemView)
        }

        inner class SongViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        }
    }
    */
    /*
    inner class ResultAdapter: RecyclerView.Adapter<ResultAdapter.ResultViewHolder>() {

        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ResultViewHolder {
            val itemView = LayoutInflater.from(p0.context).inflate(R.layout.result_list_item, p0, false)
            return ResultViewHolder(itemView)
        }

        override fun onBindViewHolder(p0: ResultViewHolder, p1: Int) {
            val product = productList[p1]
            val productImages = product.getImages()
            if (productImages.size == 0) {

            }
            else {
                Picasso.with(this@ResultListFragment.context).load(productImages[0]).into(p0.productImg)
            }
            p0.productTitle.text = product.getProductName()

            val price = product.getPrice()
            if (price == -1.0) {
                p0.productPrice.text = getString(R.string.no_pricing)
            }
            else {
                val priceString = NumberFormat.getCurrencyInstance().format(price)
                p0.productPrice.text = priceString
            }

        }

        override fun getItemCount(): Int {
            return productList.size
        }

        inner class ResultViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
            val row = itemView

            var songImg: ImageView = itemView.song_img
            var songTitle: TextView = itemView.song_title
            var songArtist: TextView = itemView.song_artist
        }
    }
    */

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(
            com.example.playlist.R.layout.fragment_main,
            container, false
        )

        //set up the songGrid as corresponding to the songGrid in the UI
        // with numCols columns, currently 2
        songGrid = view.songGrid;
        songGrid.numColumns = numCols;


        //items in the song list for debugging purpose

        /*songList?.add(PlaylistSupport.Song("Ruby", "Eric Nally", null));
        songList?.add(PlaylistSupport.Song("Bombs Away", "Foxy Shazam",
            "https://lastfm-img2.akamaized.net/i/u/34s/9e1b786043b94c634bef5a2951244565.png"));
        songList?.add(PlaylistSupport.Song("Downtown", "Macklemore", null));
        songList?.add(PlaylistSupport.Song("Rocketeer", "Foxy Shazam", null));
        songList?.add(PlaylistSupport.Song("She's Got A Way", "Billy Joel", null));
        //initialization of the songGrid as equaling the arraylist of songs
        val adapter = ArrayAdapter(context,
                    android.R.layout.simple_selectable_list_item, songList);
        songGrid.adapter = adapter;*/


        //get the top tracks using the method defined in PlaylistSupport.kt
        //here's hoping, kid
        ProductAsyncTask(this).execute();


        //shoutout to https://android--code.blogspot.com/2018/02/android-kotlin-gridview-example.html
        songGrid.onItemClickListener = object : AdapterView.OnItemClickListener {
            override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                //get the selected song
                val selectedSong: Song = parent?.getItemAtPosition(position) as Song;

                //navigate to the ViewTrack activity for this particular track
                val intent = Intent(view?.context, ViewTrack::class.java); //???????

                intent.putExtra("name", selectedSong.name);
                intent.putExtra("artist", selectedSong.artist);
                intent.putExtra("image", selectedSong.image);

                startActivity(intent);
            }
        }

        //onclicklistener for the search button
        view.searchButton.setOnClickListener {
            //first, get whatever text is in the search field
            val userInput: EditText = view.searchText;
            val query: String = userInput.text.toString();

            //all we really need to do is pretend that this is an artist
            // and update the gridview accordingly
            //this should handle all of that
            ProductAsyncTask(this).execute(query);
        }

        return view
    }


    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }


    /**
     * I don't really know what this does, but let's go for it anyway
     * Stolen from Trivia
     */
    @SuppressLint("StaticFieldLeak")
    inner class ProductAsyncTask(act: Fragment) : AsyncTask<String, Unit, java.util.ArrayList<Song>>() {
        private var act = act

        /**
         * Will need to pass in artist name as an array
         */
        override fun doInBackground(vararg params: String?): java.util.ArrayList<Song>? {
            if (params.isNotEmpty()) {
                Log.e(LogTag, params[0]);
                return PlaylistSupport(context).getTopTracks(params[0]!!);
            } else {
                return PlaylistSupport(context).getTopTracks();
            }
        }

        override fun onPostExecute(result: java.util.ArrayList<Song>?) {
            if (result == null) {
                Log.e("RESULTS", "No Results Found")
            } else {
                Log.e("RESULTS", result.toString())
                songList = ArrayList();
                for (song in result) {
                    songList?.add(song);
                }
                //initialization of the songGrid as equaling the arraylist of songs
                val adapter = ArrayAdapter(
                    context,
                    android.R.layout.simple_selectable_list_item, songList
                );
                songGrid.adapter = adapter;
            }
        }
    }
}
