package com.example.playlist

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.View
import android.widget.*
import com.example.playlist.PlaylistSupport.*
import com.example.playlist.db.DbSettings
import com.example.playlist.db.SongsDatabaseHelper
import com.squareup.picasso.Picasso

class ViewTrack : AppCompatActivity() {

    private val LogTag = this::class.java.simpleName

    private var songInfo: TextView? = null;

    private var currentSong: DenseSong? = null;


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_track)

        //get UI elements
        songInfo = findViewById<TextView>(R.id.songInfo);

        //the data about the song as passed in by the main activity
        val songName: String = intent.getStringExtra("name");
        val artist: String = intent.getStringExtra("artist");

        //now, just execute the asynctask
        //this should have constructed everything properly and set
        //currentSong equal to the result
        AnotherAsyncTask(this).execute(songName, artist);



        //Listener for the "Add To Playlist" button
        findViewById<Button>(R.id.addToPlaylist).setOnClickListener { view ->
            //prevent user from adding track already in playlist
            //first, query the whole database and check
            //see similar code in Playlist.kt
            val db = SongsDatabaseHelper(this.baseContext); //??????
            val database = db.readableDatabase;

            val query: String = "Select * from " + DbSettings.DBSongEntry.TABLE +
                    " order by " + DbSettings.DBSongEntry.ID;
            var cursor = database.rawQuery(query, null); //What the heck are selectionArgs?

//            var alreadyStored: Boolean = false;
//
//            //shoutout to JokeDB
//            with(cursor) {
//                while (moveToNext()) {
//                    //get the database entry
//                    val storedSongName = getString(getColumnIndexOrThrow(DbSettings.DBSongEntry.COL_SONGNAME))
//                    val storedArtist = getString(getColumnIndexOrThrow(DbSettings.DBSongEntry.COL_ARTIST))
//
//                    //compare it to the current selection
//                    if (storedSongName == songName && storedArtist == artist) {
//                        alreadyStored = true;
//                    }
//
//                }
//            }
            var alreadyStored: Boolean = db.isStored(Song(songName, artist, null));

            //if the song is already in the database, prevent it from being added again
            if (alreadyStored) {
                findViewById<TextView>(R.id.errorMessage).text = "Song already in playlist";
            }
            //Otherwise, add the song into the database
            else {
                db.addSong(songName, artist);

                //all we've really got to do is pass the song back along to the main activity
                // so that it can be added to the playlist fragment
                val intent = Intent(this, MainActivity::class.java);


                //pop up some delicious toast
                Toast.makeText(this, "badaboom" as CharSequence, Toast.LENGTH_SHORT).show();

                //and there it is
                //setResult(Activity.RESULT_OK, intent);
                startActivity(intent)
            }


        }

        //Listener for the "Back" button
        findViewById<Button>(R.id.returnToList).setOnClickListener { view ->
            //even easier--literally just go back after adding the
            // DO_NOTHING code to the intent
            val intent = Intent();
            intent.putExtra("action", ViewTrackReturns.DO_NOTHING);

            //and there it is
            setResult(Activity.RESULT_OK, intent);
            finish();
        }
    }


    /**
     * This is horrendously terrible, but I'm actually just going to go ahead and
     * copy-paste this from MainFrag.kt with a few minor adjustments
     */
    @SuppressLint("StaticFieldLeak")
    inner class AnotherAsyncTask(act: Activity) : AsyncTask<String, Unit, DenseSong>() {
        private var act = act

        override fun doInBackground(vararg params: String?): DenseSong? {

            return PlaylistSupport(this@ViewTrack).getTrackData(params[0]!!, params[1]!!);

        }

        override fun onPostExecute(result: DenseSong?) {
            if (result == null) {
                Log.e("RESULTS", "No Results Found")
            } else {
                Log.e("RESULTS", result.toString())
                songInfo?.text = result.toString();

                //now do that cool thing where you load the image into the imageview
                if (result.image != null) {
                    val imageView: ImageView = findViewById<ImageView>(R.id.albumCover);
                    Picasso.with(this@ViewTrack).load(result.image)
                        .resize(50, 50).centerCrop().into(imageView);
                }

                currentSong = result;

            }
        }
    }


}
