package com.example.playlist

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.example.playlist.db.SongsDatabaseHelper

class AddTags : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_tags)

        //the data about the song as passed in by the DeleteTrack activity
        val songName: String = intent.getStringExtra("songName");
        val artist: String = intent.getStringExtra("artist");

        val db = SongsDatabaseHelper(this.baseContext); //??????
        val database = db.readableDatabase;

        //listener for the add tag button
        findViewById<Button>(R.id.addTagButton).setOnClickListener {
            val newTag: String = findViewById<EditText>(R.id.enterTag).text.toString();

            //make sure they actually entered something
            if (newTag == "") {
                findViewById<TextView>(R.id.errorMessage).text = "Please enter a tag";
            }
            //if they did, add the tag to the database and navigate back to DeleteTrack
            else {
                db.addTag(songName, artist, newTag);

                //navigate to the DeleteTrack activity for this particular track
                val intent = Intent(this, DeleteTrack::class.java); //???????

                intent.putExtra("name", songName);
                intent.putExtra("artist", artist);
                //we out, buddy-boy
                startActivity(intent);
            }
        }

        //listener  for the back button
        findViewById<Button>(R.id.backButton).setOnClickListener {
            //navigate to the DeleteTrack activity for this particular track
            val intent = Intent(this, DeleteTrack::class.java); //???????

            intent.putExtra("name", songName);
            intent.putExtra("artist", artist);

            startActivity(intent);
        }
    }
}
