package com.example.playlist.db

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import com.example.playlist.PlaylistSupport

/**
 * Those familiar with JokesDB might find much of this structure somewhat familiar
 */
class SongsDatabaseHelper(context: Context): SQLiteOpenHelper(context, DbSettings.DB_NAME, null, DbSettings.DB_VERSION) {
    override fun onCreate(db: SQLiteDatabase?) {
        val createFavoritesTableQuery = "CREATE TABLE " + DbSettings.DBSongEntry.TABLE + " ( " +
                DbSettings.DBSongEntry.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DbSettings.DBSongEntry.COL_SONGNAME + " TEXT NULL, " +
                DbSettings.DBSongEntry.COL_ARTIST + " TEXT NULL, " +
                DbSettings.DBSongEntry.COL_TAGS + " TEXT NULL)"



        db?.execSQL(createFavoritesTableQuery)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL("DROP TABLE IF EXISTS " + DbSettings.DBSongEntry.TABLE)
        onCreate(db)
    }

    fun addSong(songName: String, artist: String) {
        // Gets the data repository in write mode
        val db = this.writableDatabase

// Create a new map of values, where column names are the keys
        val values = ContentValues().apply {
            put(DbSettings.DBSongEntry.COL_SONGNAME, songName)
            put(DbSettings.DBSongEntry.COL_ARTIST, artist)
        }

// Insert the new row, returning the primary key value of the new row
        val newRowId = db?.insert(DbSettings.DBSongEntry.TABLE, null, values)
    }

    fun deleteSong(songName: String, artist: String) {
        val db = this.writableDatabase;

        db.delete(DbSettings.DBSongEntry.TABLE,
            DbSettings.DBSongEntry.COL_SONGNAME + "=" + "\"" + songName + "\"" +
                    " and " + DbSettings.DBSongEntry.COL_ARTIST + "=" + "\"" + artist + "\"", null);
    }

    /**
     * Part of the creative portion; adds a new tag onto the list of tags
     */
    fun addTag(songName: String, artist: String, newTag: String) {
        val db = this.writableDatabase;

        var oldTags = getTags(songName, artist);

        //if there were no old tags, we can just make the set of new tags
        // exactly equal to the passed-in entry
        var tags: String = "";
        if (oldTags != null) {
            //we're just storing this as a comma-separated list of values,
            // nothing fancy
            tags = oldTags + "," + newTag;
        }
        else {
            tags = newTag;
        }
        //now we add it back into the database using, you guessed it,
        // another raw query
        /*val putQuery: String = "Update " + DbSettings.DBSongEntry.TABLE +
                " set " + DbSettings.DBSongEntry.COL_TAGS + "="  + "\"" + tags + "\"" +
                " where " + DbSettings.DBSongEntry.COL_SONGNAME + "=" + "\"" + songName + "\"" +
                " and " + DbSettings.DBSongEntry.COL_ARTIST + "="  + "\"" + artist + "\"";
        db.rawQuery(putQuery, null);
        */
        val content: ContentValues = ContentValues();
        //map column name to new column value
        content.put(DbSettings.DBSongEntry.COL_TAGS, tags);

        db.update(
            DbSettings.DBSongEntry.TABLE,
            content,
            DbSettings.DBSongEntry.COL_SONGNAME + "=" + "\"" + songName + "\"" +
                    " and " + DbSettings.DBSongEntry.COL_ARTIST + "="  + "\"" + artist + "\"",
            null
        );
    }

    /**
     * Gets the current custom tags for the specified songs
     * Also invoked in DeleteTrack
     */
    fun getTags(songName: String, artist: String) : String? {
        val db = this.writableDatabase;

        //again, I feel like it's easier to just do a raw query here
        // to retrieve and modify the data
        val getQuery: String = "Select " + DbSettings.DBSongEntry.COL_TAGS +
                " from  "+ DbSettings.DBSongEntry.TABLE +
                " where " + DbSettings.DBSongEntry.COL_SONGNAME + "=" + "\"" + songName + "\"" +
                " and " + DbSettings.DBSongEntry.COL_ARTIST + "="  + "\"" + artist + "\"";
        var cursor = db.rawQuery(getQuery, null);

        var oldTags: String? = null;
        //there should be just the one entry
        with(cursor) {
            while (moveToNext()) {

                oldTags = getString(getColumnIndexOrThrow(DbSettings.DBSongEntry.COL_TAGS))
                //this is the worst thing I've ever done
            }
        }

        return oldTags;
    }

    /**
     * Part of the creative portion--gets a set of the artists
     * that are represented in the database for hte purpose of creating
     * song recommendations
     */
    fun getArtists() : Set<String> {
        val db = this.readableDatabase;
        val artists: HashSet<String> = HashSet<String>();

        val getQuery: String = "Select " + DbSettings.DBSongEntry.COL_ARTIST +
                " from " + DbSettings.DBSongEntry.TABLE;
        var cursor = db.rawQuery(getQuery, null);

        //pretty straightforward, add everything to the set
        with(cursor) {
            while (moveToNext()) {
                val artist: String = getString(getColumnIndexOrThrow(DbSettings.DBSongEntry.COL_ARTIST));
                artists.add(artist);
            }
        }

        return artists;
    }

    /**
     * A handy little method checking whether a given song is
     * already stored in the database
     */
    fun isStored(song: PlaylistSupport.Song) : Boolean {
        var alreadyStored: Boolean = false;
        val db = this.readableDatabase;

        val songName: String = song.name;
        val artist: String = song.artist;

        //I couldn't figure out how to use the Kotlin query function, so I just did a raw query
        //shoutout to https://www.quora.com/Which-is-the-best-way-to-read-data-from-an-SQLite-Database-in-Android
        val query: String = "Select * from " + DbSettings.DBSongEntry.TABLE +
                " order by " + DbSettings.DBSongEntry.ID;
        var cursor = db.rawQuery(query, null); //What the heck are selectionArgs?
        //shoutout to JokeDB
        with(cursor) {
            while (moveToNext()) {
                //get the database entry
                val storedSongName = getString(getColumnIndexOrThrow(DbSettings.DBSongEntry.COL_SONGNAME))
                val storedArtist = getString(getColumnIndexOrThrow(DbSettings.DBSongEntry.COL_ARTIST))

                //compare it to the current selection
                if (storedSongName == songName && storedArtist == artist) {
                    alreadyStored = true;
                }

            }
        }

        return alreadyStored;
    }

    /**
     * Called on startup of the Playlist fragment, adds all of the songs
     * in the database to the specified arraylist
     */
    fun addSongsToList(playlist: ArrayList<PlaylistSupport.Song>) {
        //now get the elements out of the database
        val db = this.readableDatabase;
        //here we go
        //I couldn't figure out how to use the Kotlin query function, so I just did a raw query
        //shoutout to https://www.quora.com/Which-is-the-best-way-to-read-data-from-an-SQLite-Database-in-Android
        val query: String = "Select * from " + DbSettings.DBSongEntry.TABLE +
                " order by " + DbSettings.DBSongEntry.ID;
        var cursor = db.rawQuery(query, null); //What the heck are selectionArgs?

        //shoutout to JokeDB
        with(cursor) {
            while (moveToNext()) {
                val songName = getString(getColumnIndexOrThrow(com.example.playlist.db.DbSettings.DBSongEntry.COL_SONGNAME))
                val artist = getString(getColumnIndexOrThrow(com.example.playlist.db.DbSettings.DBSongEntry.COL_ARTIST))
                val song = com.example.playlist.PlaylistSupport.Song(songName, artist, null)
                playlist.add(song)
            }
        }
    }

}