package com.example.playlist


import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import com.example.playlist.PlaylistSupport.*
import com.example.playlist.db.DbSettings
import com.example.playlist.db.SongsDatabaseHelper
import kotlinx.android.synthetic.main.fragment_playlist.view.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class Playlist : Fragment() {

    private var playlist: ArrayList<Song> = ArrayList();
    private lateinit var playlistView : ListView;

    //????????
    private lateinit var db: SongsDatabaseHelper;

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_playlist, container, false)

        playlistView = view.findViewById<ListView>(R.id.playlist);


        //On item click listener!  Hooray!
        //looks very similar to code in main fragment
        playlistView.onItemClickListener = object: AdapterView.OnItemClickListener {
            override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                //get the selected song
                val selectedSong: Song = parent?.getItemAtPosition(position) as Song;

                //navigate to the DeleteTrack activity for this particular track
                val intent = Intent(view?.context, DeleteTrack::class.java); //???????

                intent.putExtra("name", selectedSong.name);
                intent.putExtra("artist", selectedSong.artist);
                intent.putExtra("image", selectedSong.image);

                startActivity(intent);
            }
        }

        //click listener for the song recommendation button
        view.recommendSong.setOnClickListener {
            //all we really need to do is execute the ProductAsyncTask
            // thing, which does everything for us
            ProductAsyncTask(this).execute();
        }


        db = SongsDatabaseHelper(this.context as Context);

        return view;
    }

    override fun onStart() {
        super.onStart();

        //I don't really know, but let's try just retrieving stuff
        //this is probably the worst way in the world of doing this,
        // but here goes

        //eliminate all the elements in playlist
        playlist = arrayListOf();

        //adds all of the songs stored in the database to the arraylist
        db.addSongsToList(playlist);

        //now we display that by changing the adapter
        playlistView.adapter = ArrayAdapter(this.context as Context,
            android.R.layout.simple_list_item_1, playlist);

    }

    /**
     * Part of the creative portion--an asynchronous task that will
     * get song recommendations for the user based on the songs that
     * they've already added to the playlist
     * Almost entirely copy-pasted from the main fragment, which, in turn,
     * is almost entirely copy-pasted from the Trivia app
     */
    @SuppressLint("StaticFieldLeak")
    inner class ProductAsyncTask(act: Fragment) : AsyncTask<String, Unit, Song?>() {
        private var act = act

        override fun doInBackground(vararg params: String?): Song? {
            return getSongRec();
        }

        /**
         * Mostly, we want to go to the ViewTrack
         * page for the returned song, giving the
         * user the option to add it to the playlist
         */
        override fun onPostExecute(result: Song?) {
            if (result == null) {
                Log.e("RESULTS", "No Results Found")
            } else {
                val intent = Intent(context, ViewTrack::class.java);

                intent.putExtra("name", result.name);
                intent.putExtra("artist", result.artist);

                startActivity(intent);
            }
        }
    }

    /**
     * Part of the creative portion--a button will allow people to get
     * a song recommendation based on the artists to whom they like to listen
     */
    private fun getSongRec() : Song? {
        val db: SongsDatabaseHelper = SongsDatabaseHelper(this.context as Context);
        var rec: Song? = null;

        //get all of the artists represented so far
        val favArtists: Set<String> = db.getArtists();

        //now go through and make API queries for each of them
        // in an arbitrary order
        for (artist: String in favArtists) {
            rec = getTrackNotInDatabase(artist);
            //if anything was found for this artist, return it
            if (rec != null) {
                return rec;
            }
        }
        //we'll only reach this point if there are no remaining
        // top tracks by any artist already in the database
        //now, we get to go to defaults
        //plus a little insight into my music choices

        //an amazing experimental punk group, cross between Queen
        // and Ozzy Osborne
        val default1: String = "Foxy Shazam";
        //a classic
        val default2: String = "Billy Joel";
        //an incredible vocal percussionist and a cappella/electronic
        // musician, he just released a great new album called Transcend
        val default3: String = "Gene Shinozaki";

        rec = getTrackNotInDatabase(default1);
        if (rec != null) {
            return rec;
        }
        rec = getTrackNotInDatabase(default2);
        if (rec != null) {
            return rec;
        }
        rec = getTrackNotInDatabase(default3);
        if (rec != null) {
            return rec;
        }

        //I give up, just return null
        return rec;
    }

    /**
     * Called several times in the method above
     */
    private fun getTrackNotInDatabase(artist: String) : Song? {
        //make an API query to get a list of the artist's top tracks
        val topTracks: ArrayList<Song>? = PlaylistSupport().getTopTracks(artist);

        //now, find the first track that is not already in the database and return it
        if (topTracks == null) {
            //if no top tracks were found, whatever
            return null;
        }
        for (song: Song in topTracks) {
            //if it's not already in the database, we're done
            if (!db.isStored(song)) {
                return song;
            }
        }

        //nothing has been found
        return null;
    }


}
