package com.example.playlist.db

import android.provider.BaseColumns

/**
 * See the similar class from our favorite JokeDB
 */
class DbSettings {
    companion object {
        const val DB_NAME = "playlist.db"
        //see how we're on version 2?  I was kind of proud of myself
        // for figuring that one out
        const val DB_VERSION = 2
    }

    class DBSongEntry: BaseColumns {
        companion object {
            const val TABLE = "playlist"
            const val ID = BaseColumns._ID
            const val COL_SONGNAME = "songName"
            const val COL_ARTIST = "artist"

            //part of my creative portion--user can add in their own
            // custom tags
            const val COL_TAGS = "tags"
        }
    }
}