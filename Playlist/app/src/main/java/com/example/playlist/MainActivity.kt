package com.example.playlist

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.GridView
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import kotlinx.android.synthetic.main.activity_main.*
import com.example.playlist.PlaylistSupport.*



/**
 * API details
 * API key: ba811938db6734ba9a88abed982eaabc
 * Shared secret: a1c08a94cd870a433b992aa032514d0a
 * Registered to: romanaccardi
 *
 *
 * Results taken from Last.fm - big shoutout to that API
 */

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //I was afraid of this
        //adapted from the JokeDB
        val fragmentAdapter = MyPagerAdapter(supportFragmentManager)
        viewpager_main.adapter = fragmentAdapter

        tabs_main.setupWithViewPager(viewpager_main)

        //this seems important
        tabs_main.setupWithViewPager(viewpager_main)

    }


    /**
     * An inner class stolen wholesale from JokeDB
     */
    class MyPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            return when (position) {
                0 -> {
                    TopTracks()
                }
                else -> Playlist()
            }
        }

        override fun getCount(): Int {
            return 2
        }

        override fun getPageTitle(position: Int): CharSequence {
            return when (position) {
                0 -> "Top Tracks"
                else -> {
                    return "Your Playlist"
                }
            }
        }
    }
}
