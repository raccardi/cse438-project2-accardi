package com.example.playlist

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.example.playlist.PlaylistSupport.*
import com.example.playlist.db.DbSettings
import com.example.playlist.db.SongsDatabaseHelper
import com.squareup.picasso.Picasso

/**
 * This class has the dubious distinction of being nearly identical to
 * ViewTrack
 * The only real difference is that, instead of adding things to the playlist,
 * this has a button to delete stuff from the playlist
 */
class DeleteTrack : AppCompatActivity() {

    private val LogTag = this::class.java.simpleName

    private var songInfo: TextView? = null;

    private var currentSong: DenseSong? = null;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_delete_track)


        //get UI elements
        songInfo = findViewById<TextView>(R.id.songInfo);

        //the data about the song as passed in by the main activity
        val songName: String = intent.getStringExtra("name");
        val artist: String = intent.getStringExtra("artist");

        //now, just execute the asynctask
        //this should have constructed everything properly and set
        //currentSong equal to the result
        AnotherAsyncTask(this).execute(songName, artist);


        //Listener for the "Add To Playlist" button
        findViewById<Button>(R.id.deleteFromPlaylist).setOnClickListener { view ->
            //prevent user from adding track already in playlist
            //first, query the whole database and check
            //see similar code in Playlist.kt
            val db = SongsDatabaseHelper(this.baseContext); //??????
            val database = db.readableDatabase;


            //Otherwise, add the song into the database
            db.deleteSong(songName, artist);

            //all we've really got to do is pass the song back along to the main activity
            // so that it can be added to the playlist fragment
            val intent = Intent(this, MainActivity::class.java);


            //pop up some delicious toast
            Toast.makeText(this, "badaboom" as CharSequence, Toast.LENGTH_SHORT).show();

            //and there it is
            //setResult(Activity.RESULT_OK, intent);
            startActivity(intent)


        }

        //Listener for the "Back" button
        findViewById<Button>(R.id.returnToList).setOnClickListener { view ->
            //even easier--literally just go back after adding the
            // DO_NOTHING code to the intent
            val intent = Intent(this, MainActivity::class.java);

            //and there it is
            startActivity(intent);
        }

        //Listener for the add tags button
        findViewById<Button>(R.id.addTags).setOnClickListener { view ->


            val intent = Intent(this, AddTags::class.java);
            //helpful for identifying the song to which to add
            // the new tag
            intent.putExtra("songName", currentSong?.name);
            intent.putExtra("artist", currentSong?.artist);
            //and navigate on over to that activity
            startActivity(intent);
        }
    }

    /**
     * A precise copy-paste from ViewTrack
     */
    @SuppressLint("StaticFieldLeak")
    inner class AnotherAsyncTask(act: Activity) : AsyncTask<String, Unit, DenseSong>() {
        private var act = act

        override fun doInBackground(vararg params: String?): DenseSong? {

            return PlaylistSupport(this@DeleteTrack).getTrackData(params[0]!!, params[1]!!);

        }

        override fun onPostExecute(result: DenseSong?) {
            if (result == null) {
                Log.e("RESULTS", "No Results Found")
            } else {
                Log.e("RESULTS", result.toString())

                //get all the tags from the database
                val db: SongsDatabaseHelper = SongsDatabaseHelper(this@DeleteTrack);
                val tags: String? = db.getTags(result.name, result.artist);

                Log.e(LogTag, "" + tags);

                //if we found any, go ahead and concatenate them onto the end
                // of the tags that we got from the API
                if (tags != null) {
                    //the custom tags are all comma-separated
                    val customTags = tags.split(",");
                    for (tag: String in customTags) {
                        result.listOfTags.add(tag);
                    }
                }

                songInfo?.text = result.toString();

                //now do that cool thing where you load the image into the imageview
                if (result.image != null) {
                    val imageView: ImageView = findViewById<ImageView>(R.id.albumCover);
                    Picasso.with(this@DeleteTrack).load(result.image)
                        .resize(50, 50).centerCrop().into(imageView);
                }

                currentSong = result;

            }
        }
    }



}
