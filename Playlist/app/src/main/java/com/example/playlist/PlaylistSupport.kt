package com.example.playlist

import android.content.Context
import android.text.TextUtils
import android.util.Log
import android.widget.Toast
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import java.nio.charset.Charset


class PlaylistSupport(context: Context? = null) {

    private val currentContext = context;

    private val LogTag = this::class.java.simpleName

    //the maximum number of songs that you won't to load at
    // any given moment
    val maxSongs: Int = 20;

    /**
     * I don't really know what a companion object is,
     * but my understanding is that it's essentially a static
     * class.
     * Storing all of the important API access details in here
     */
    companion object ApiDetails {
        private val apiKey: String = "ba811938db6734ba9a88abed982eaabc";
        private val sharedSecret: String = "a1c08a94cd870a433b992aa032514d0a";
        private val username: String = "romanaccardi";
        private val baseURL: String = "http://ws.audioscrobbler.com/2.0/";
    }

    /**
     * A  class used to represent a song
     * contains a name, an artist, and an image that will be represented
     * somehow, I guess
     * Public so that the class is also accessible in the ViewTrack activity
     */
    public class Song constructor(name: String, artist: String, image: String?) {
        val name = name;
        val artist = artist;
        val image = image;

        override fun toString(): String {
            //TODO - what to do about image
            return name + "\nArtist: " + artist;
        }
    }

    /**
     * This is a more dense representation of a song, with some other data
     * used for display in ViewTrack and DeleteTrack
     * Specifically, DenseSong contains
     *  1) the song name
     *  2) the artist
     *  3) an image associated with the song
     *  4) the album title
     *  5) the number of times the song has been played
     *  6) a list of tags associated with the song
     */
    public class DenseSong constructor(
        name: String, artist: String, image: String?,
        albumTitle: String, numPlays: String,
        listOfTags: ArrayList<String>
    ) {
        val name = name;
        val artist = artist;
        val image = image;
        val albumTitle = albumTitle;
        val numPlays = numPlays;
        val listOfTags = listOfTags;

        override fun toString(): String {
            var stringTo: String = name +
                    "\nArtist: " + artist +
                    "\nAlbum: " + albumTitle +
                    "\nPlayed " + numPlays + " times" +
                    "\nTags: ";
            //iterate through and add all of the tags
            for (tag in listOfTags) {
                stringTo += tag + ",";
            }
            //get rid of that last pesky comma
            stringTo = stringTo.subSequence(0, stringTo.length - 1) as String;

            return stringTo;
        }
    }

    /**
     * A simple enumeration of the possible values
     * that ViewTrack can return to the main activity
     */
    public enum class ViewTrackReturns {
        ADD_SONG,
        DO_NOTHING
    }

    /**
     * A simple enumeration of the different possible sizes of picture
     * based on where they would be situated in the JSON object
     */
    private enum class ImageSizes(val value: Int) {
        SMALL(0),
        MEDIUM(1),
        LARGE(2),
        VERY_LARGE(3)
    }


    /**
     * Below are three functions.  The first two functions,
     * getTopTracks and getTrackData,
     * are all invoked at various points in the activities
     * and fragments in order to make API calls.
     *
     * Since the code for getting the top tracks and getting
     * an individual artist's top tracks
     * is going to be pretty much the same, both of those
     * invoke the getTracks function that operates on the
     * JSON returned by the URL to get the requested
     * track list
     *
     * Thus, you can pass the artist name in as a parameter, or you
     * can simply leave it off, and it will default to getting the
     * top tracks in general
     */
    private val noArtist: String = "NO ARTIST SPECIFIED";

    public fun getTopTracks(artistName: String = noArtist): ArrayList<Song>? {
        var url: URL? = null;

        //if no artist name is specified, just get the top tracks
        // in general
        val searchByArtist: Boolean = artistName != noArtist;
        if (!searchByArtist) {
            url = URL(
                ApiDetails.baseURL
                        + "?method=chart.gettoptracks&api_key=" + ApiDetails.apiKey
                        + "&format=json"
            );
        }
        else {
            //first, sanitize the artist name by replacing
            // spaces with '+'
            artistName.replace(' ', '+');

            //then construct the URL
            url = URL(
                ApiDetails.baseURL
                        + "?method=artist.gettoptracks&artist=" + artistName
                        + "&api_key=" + ApiDetails.apiKey
                        + "&format=json"
            );
        }

        return getTracks(url, searchByArtist);
    }


    //Method used to get either the top tracks in general, or top tracks by
    // a specific artist, depending on what boolean is passed in
    public fun getTracks(url: URL?, searchByArtist: Boolean): ArrayList<Song>? {
        var topTrackz: ArrayList<Song> = ArrayList<Song>();

        //get the JSON response as a string
        //literally all of this code is from studio 3
        var jsonResponse: String? = null;
        try {
            jsonResponse = makeHttpRequest(url);
        } catch (e: IOException) {
            Log.e(this.LogTag, "Could not make HTTP request");
        }

        //got to make sure we're not doing anything crazy
        if (TextUtils.isEmpty(jsonResponse)) {
            Log.e(this.LogTag, "Could not retrieve from this URL: " + url.toString());
            return null;
        }

        //see studio 3
        try {
            val baseJsonResponse = JSONObject(jsonResponse);
            //because of the way this is structured, the actual list of
            // tracks is a subfield within the larger object
            //and even that is a subfield of "tracks!"
            //good gracious
            //so it goes "tracks"{"track"{...}}
            //OR "toptracks"{"track"{...}} if we're searching by artist

            var jsonFirstLayer: JSONObject? = null;

            if (searchByArtist) {
                jsonFirstLayer = returnValueOrDefault<JSONObject>(
                    baseJsonResponse, "toptracks"
                ) as JSONObject;
            }
            else {
                jsonFirstLayer = returnValueOrDefault<JSONObject>(
                    baseJsonResponse, "tracks"
                ) as JSONObject;
            }

            val jsonTrackList: JSONArray = returnValueOrDefault<JSONArray>(
                jsonFirstLayer as JSONObject, "track"
            ) as JSONArray;


            for (i in 0 until jsonTrackList.length()) {

                //so, we want to stop if we get to a certain value
                //this is a very inelegant way of doing so, but what can you do
                if (i >= maxSongs) {
                    break;
                }

                val trackObject = jsonTrackList.getJSONObject(i);

                //////TRACK
                val trackNameString: String = returnValueOrDefault<String>(
                    trackObject as JSONObject, "name"
                ) as String;

                //////ARTIST
                //first get the object full of artist data
                val artistData: JSONObject? = returnValueOrDefault<JSONObject>(trackObject, "artist")
                        as JSONObject;
                //then extract the name in specific therefrom
                val artistNameString: String = returnValueOrDefault<String>(
                    artistData as JSONObject, "name"
                ) as String;

                //////IMAGE
                //the images are actually stored as a list, so get them as a JSONArray first
                val imageList: JSONArray? = returnValueOrDefault<JSONArray>(trackObject, "image")
                        as JSONArray;
                //now retrieve the appropriate image from that list (see enum class above)
                val imageJSONObject: JSONObject? = imageList?.getJSONObject(ImageSizes.MEDIUM.value);
                //and get the actual URL from that object
                val imageUrlString = returnValueOrDefault<String>(
                    imageJSONObject
                            as JSONObject, "#text"
                ) as String;

                //now we can construct a song object from the retrieved data and add
                // it to our array
                val topSong: Song = Song(trackNameString, artistNameString, imageUrlString);
                topTrackz.add(topSong);
            }
        } catch (e: JSONException) {
            Log.e(this.LogTag, "Problem parsing JSON");
        }


        //array has been filled
        return topTrackz;
    }

    /**
     * Get the data for a specific track, called in ViewTrack
     * and DeleteTrack to display further information
     */
    public fun getTrackData(trackName: String, artistName: String): DenseSong? {
        var track: DenseSong? = null;

        //construct the URL with the specified data
        val url: URL? = URL(
            ApiDetails.baseURL
                    + "?method=track.getInfo&artist=" + artistName
                    + "&api_key=" + ApiDetails.apiKey
                    + "&track=" + trackName
                    + "&format=json"
        );

        //get the JSON response as a string
        //literally all of this code is from studio 3
        //and much of it is a direct repeat of the getTracks function
        var jsonResponse: String? = null;
        try {
            jsonResponse = makeHttpRequest(url);
        } catch (e: IOException) {
            Log.e(this.LogTag, "Could not make HTTP request");
        }
        //got to make sure we're not doing anything crazy
        if (TextUtils.isEmpty(jsonResponse)) {
            return null;
        }

        //then parse the data from the JSON
        try {
            val baseJsonResponse = JSONObject(jsonResponse);
            val trackObject: JSONObject = returnValueOrDefault<JSONObject>(
                baseJsonResponse, "track"
            ) as JSONObject;

            //get all of the data
            //we already know songname and artist name
            //also find:
            // 1) image
            // 2) album title
            // 3) playcount
            // 4) list of tags
            //get the album object from the track
            //it turns out that album is not a required field,
            // so this is all nullable
            val albumObject: JSONObject? = returnValueOrDefault<JSONObject>(
                trackObject, "album"
            ) as JSONObject?;

            var imageUrlString: String? = null;
            lateinit var albumTitleString: String;
            //in case it's null, we can't do anything with the image
            // or album title, and thus have to assign default values
            if (albumObject != null) {
                //////IMAGE
                //the images are actually stored as a list under "album", so get them as a JSONArray first
                val imageList: JSONArray? = returnValueOrDefault<JSONArray>(albumObject, "image")
                        as JSONArray;
                //now retrieve the appropriate image from that list (see enum class above)
                val imageJSONObject: JSONObject? = imageList?.getJSONObject(ImageSizes.LARGE.value);
                //and get the actual URL from that object
                imageUrlString = returnValueOrDefault<String>(
                    imageJSONObject
                            as JSONObject, "#text"
                ) as String;

                //////ALBUM TITLE
                albumTitleString = returnValueOrDefault<String>(
                    albumObject, "title"
                ) as String;
            }
            //if there is no album, we're talking default value city here
            else {
                imageUrlString = null;
                albumTitleString = "No album listed";
            }



            //////PLAYCOUNT
            val playCountString: String = returnValueOrDefault<String>(
                trackObject, "playcount"
            ) as String;

            //////LIST OF TAGS
            //retrieve the list of tags from the JSON object
            val topTags: JSONObject = returnValueOrDefault<JSONObject>(
                trackObject, "toptags"
            ) as JSONObject;
            //it's a two-step process, in case you couldn't tell
            val tagJsonArray: JSONArray = returnValueOrDefault<JSONArray>(
                topTags, "tag"
            ) as JSONArray;

            //and now we have to get the actual tag values from the JSONArray and
            // add them to an array list
            val tags: ArrayList<String> = ArrayList();
            for (i in 0 until tagJsonArray.length()) {
                //iterate through and add each tag to the list
                tags.add(
                    returnValueOrDefault<String>(
                        tagJsonArray.getJSONObject(i), "name"
                    ) as String
                );
            }
            //tags now contains a list of all of the tags


            //with all that data above, we can construct
            // a new dense song object to store it all
            track = DenseSong(trackName,
                artistName,
                imageUrlString,
                albumTitleString,
                playCountString,
                tags
            );


        } catch (e: JSONException) {
            Log.e(this.LogTag, "Problem parsing JSON");
        }

        return track;
    }


    /**
     * This method is stolen in its entirety from the Trivia app
     *
     */
    private fun makeHttpRequest(url: URL?): String {
        var jsonResponse = ""

        if (url == null) {
            return jsonResponse
        }

        var urlConnection: HttpURLConnection? = null
        var inputStream: InputStream? = null
        try {
            urlConnection = url.openConnection() as HttpURLConnection
            urlConnection.readTimeout = 10000 // 10 seconds
            urlConnection.connectTimeout = 15000 // 15 seconds
            urlConnection.requestMethod = "GET"
            urlConnection.connect()

            if (urlConnection.responseCode == 200) {
                inputStream = urlConnection.inputStream
                jsonResponse = readFromStream(inputStream)
            } else {
                Log.e(this.LogTag, "Error response code: ${urlConnection.responseCode}")
            }
        } catch (e: IOException) {
            Log.e(this.LogTag, "Problem with HTTP request: $url", e)
        } finally {
            urlConnection?.disconnect()
            inputStream?.close()
        }

        return jsonResponse
    }

    /**
     * Also stolen from the Trivia app
     */
    private fun readFromStream(inputStream: InputStream?): String {
        val output = StringBuilder()
        if (inputStream != null) {
            val inputStreamReader = InputStreamReader(inputStream, Charset.forName("UTF-8"))
            val reader = BufferedReader(inputStreamReader)
            var line = reader.readLine()
            while (line != null) {
                output.append(line)
                line = reader.readLine()
            }
        }

        return output.toString()
    }

    /**
     * I'm starting to feel bad about just stealing all of this
     * from studio 3 and the trivia app, but here we go
     */
    private inline fun <reified T> returnValueOrDefault(json: JSONObject, key: String): Any? {
        when (T::class) {
            String::class -> {
                return if (json.has(key)) {
                    json.getString(key)
                } else {
                    ""
                }
            }
            Int::class -> {
                return if (json.has(key)) {
                    json.getInt(key)
                } else {
                    return -1
                }
            }
            Double::class -> {
                return if (json.has(key)) {
                    json.getDouble(key)
                } else {
                    return -1.0
                }
            }
            Long::class -> {
                return if (json.has(key)) {
                    json.getLong(key)
                } else {
                    return (-1).toLong()
                }
            }
            JSONObject::class -> {
                return if (json.has(key)) {
                    json.getJSONObject(key)
                } else {
                    return null
                }
            }
            JSONArray::class -> {
                return if (json.has(key)) {
                    json.getJSONArray(key)
                } else {
                    return null
                }
            }
            else -> {
                return null
            }
        }
    }
}
