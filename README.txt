Author: Roman Accardi

========================================================
					Acknowledgements
========================================================

Big shoutout to Last.fm, whose API is used in this app.
I also have to acknowledge JokeDB, the Trivia App, and Studio 3,
from which I appropriated a great deal of the code (see inline
commented citations for details)
Any further inspirations are noted in comments throughout the code.

========================================================
						Notes
========================================================

Like last time, I have been as yet unable to get a working emulator,
so everything is sized to fit my own Samsung phone.

Also, I should note that displaying images definitely does work
whenever you're viewing a track, but for some reason most of the Ariana
Grande tracks do not have albums, and thus do not have images associated
with them in individual API calls.  When there is an album, and thus an
album cover, associated with a track, then everything works as expected.

========================================================
					Creative Portion
========================================================

My creative portion consisted of two parts.

====================
Song Recommendations
====================

If a user can't decide what they want to listen to next, they can click
on "Get Song Recommendation" in the Playlist fragment.  This will take
an arbitrary song already in their playlist and recommend a different
song by that same artist, giving them the opportunity to add said song
to their playlist.

===============
Add custom tags
===============

After adding a song to their playlist, a user can click on said song
in order to access more functionality.  When they do, they will find
an option to add a new tag.  The new added tag(s) will be stored in the
SQLite database and displayed each time that they access that element
of their playlist.

It might not sound impressive, but given the amount of time I spent
wrangling the SQLite database I think it practically deserves
extra credit :D

(10 / 10 points) The app displays the current top tracks in a GridView on startup
(10 / 10 points) The app uses a tab bar with two tabs, one for searching for tracks and one for looking at the playlist
(10 / 10 points) Data is pulled from the API and processed into a GridView on the main page. Makes use of a Fragment to display the results seamlessly.
(15 / 15 points) Selecting a track from the GridView opens a new activity with the track cover, title, and 3 other pieces of information as well as the ability to save it to the playlist.
(5 / 5 points) User can change search query by editing text field.
(10 / 10 points) User can save a track to their playlist, and the track is saved into a SQLite database.
(5 / 5 points) User can delete a track from the playlist (deleting it from the SQLite database itself).
(4 / 4 points) App is visually appealing
(1/ 1 point) Properly attribute Last.fm API as source of data.
(5 / 5 points) Code is well formatted and commented.
(10 / 10 points) All API calls are done asynchronously and do not stall the application.
(15 / 15 points) Creative portion: Be creative!

Total: 100 / 100

This is fantastic! Everything works well and I can tell that you put a lot of effort into your creative portion. Great job!